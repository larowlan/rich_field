<?php

namespace Drupal\rich_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rich_field\Plugin\Field\RichFieldTrait;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'rich_field' widget.
 *
 * @todo
 * - Implement the rest of the interface methods.
 * - Support per-subfield labels.
 * - Support per-subfield help text.
 * - Support selection of the subfield widgets.
 *
 * @FieldWidget(
 *   id = "rich_field",
 *   module = "rich_field",
 *   label = @Translation("Rich Field"),
 *   field_types = {
 *     "rich_field"
 *   }
 * )
 */
class RichWidget extends WidgetBase {

  use RichFieldTrait;

  /**
   * The subfield widget instances.
   *
   * @var \Drupal\Core\Field\WidgetInterface[]
   */
  protected $subfields = [];

  /**
   * Provides the field widget manager service.
   *
   * @return \Drupal\Core\Field\WidgetPluginManager
   *   The field widget manager service.
   */
  protected static function fieldPluginManager() {
    static $manager;
    return $manager ?? $manager = \Drupal::service('plugin.manager.field.widget');
  }

  /**
   * {@inheritdoc}
   */
  protected static function getSubfieldTypes() {
    return [
      'foo' => 'string_textfield',
      'bar' => 'telephone_default',
      'bat' => 'link_default',
      'baz' => 'entity_reference_autocomplete',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    // Instantiate subfield widgets.
    foreach (static::getSubfieldTypes() as $name => $type) {
      $this->subfields[$name] = static::fieldPluginManager()
        ->createInstance($type, [
          'third_party_settings' => $this
            ->getThirdPartySettings(),
          'type' => $type,
          'settings' => static::normalizeKeys($this
            ->getSettings(), $name),
          'field_definition' => static::getSubfieldDefinition($field_definition, $name),
        ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return static::union(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return $this->instanceUnion(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $subelements = [];
    foreach ($this->subfields as $name => $subfield) {
      $normalized_items = static::normalizeItems($items, $name);
      $subelements[$name] = $subfield
        ->formElement($normalized_items, $delta, $element, $form, $form_state);
    }
    return $subelements + $element;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Settings values not saving (did work at one point!).
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return $this
      ->unionWithForm(__FUNCTION__, [
        'fields',
        $this->fieldDefinition->getName(),
        'settings_edit_form',
        'settings',
      ], $form, $form_state);
  }

}
