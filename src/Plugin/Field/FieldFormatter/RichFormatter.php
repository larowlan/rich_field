<?php

namespace Drupal\rich_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\rich_field\Plugin\Field\RichFieldTrait;

/**
 * Plugin implementation of the 'rich_field' formatter.
 *
 * @todo
 * - Implement the rest of the interface methods.
 * - Support selection of the subfield formatters.
 *
 * @FieldFormatter(
 *   id = "rich_field",
 *   label = @Translation("Rich Field"),
 *   field_types = {
 *     "rich_field"
 *   }
 * )
 */
class RichFormatter extends FormatterBase {

  use RichFieldTrait;

  /**
   * The subfield formatter instances.
   *
   * @var \Drupal\Core\Field\FormatterInterface[]
   */
  protected $subfields = [];

  /**
   * {@inheritdoc}
   */
  protected static function getSubfieldTypes() {
    return [
      'foo' => 'string',
      'bar' => 'telephone_link',
      'bat' => 'link',
      'baz' => 'entity_reference_label',
    ];
  }

  /**
   * Provides the field formatter manager service.
   *
   * @return \Drupal\Core\Field\FormatterPluginManager
   *   The field formatter manager service.
   */
  public static function fieldPluginManager() {
    static $manager;
    return $manager ?? $manager = \Drupal::service('plugin.manager.field.formatter');
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    // Set the subfield formatter instances.
    foreach (static::getSubfieldTypes() as $name => $type) {
      $subfield_definition = static::getSubfieldDefinition($field_definition, $name);
      $this->subfields[$name] = static::fieldPluginManager()
        ->createInstance($type, [
          'type' => $type,
          'label' => $this->label,
          'view_mode' => $this->viewMode,
          'third_party_settings' => $this
            ->getThirdPartySettings(),
          'settings' => $subfield_definition
            ->getSettings(),
          'field_definition' => $subfield_definition,
        ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return static::union(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return $this->instanceUnion(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    foreach ($this->subfields as $name => $subfield) {
      $subitems = static::normalizeItems($items, $name);
      for ($delta = 0; $delta < count($items); $delta++) {
        $element[$delta][$name] = $subfield
          ->viewElements($subitems, $langcode);
      }
    }
    return $element ?? NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Settings values not saving.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return $this
      ->unionWithForm(__FUNCTION__, [
        'fields',
        $this->fieldDefinition->getName(),
        'settings_edit_form',
        'settings',
      ], $form, $form_state);
  }

}
